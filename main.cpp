#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>
#include "Arquivos.h"
#include "FuncoesVectors.h"
#include "FuncoesMaqTuring.h"

using namespace std;

int main()
{
    vector<string> turing;
    vector<string> alfabetoDeEntrada;
    vector<string> alfabetoDeFita;
    string inicial,efinal;
    vector<string> transicoes;
    vector<string> fitaDeResposta;
    vector<string> palavra;
    CarregaArq(turing);
    Vector(turing,alfabetoDeEntrada,alfabetoDeFita,inicial,efinal,transicoes,palavra);
    ChamaPalavra(palavra);
    MaquinaTuring(turing,alfabetoDeEntrada,alfabetoDeFita,inicial,efinal,transicoes,fitaDeResposta,palavra);
}
