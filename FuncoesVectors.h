#ifndef FUNCOESVECTORS_H_INCLUDED
#define FUNCOESVECTORS_H_INCLUDED


///Aquivo com funcoes que separa os dados, tirados do arquivo em diversos vector para maior facilidade de manipulacao
void Vector(vector<string> &turing, vector<string> &alfabetoDeEntrada, vector<string> &alfabetoDeFita, string& inicial,
        string& efinal, vector<string> &transicoes,vector<string> &palavra) {

    string aux;
    aux = turing[0];
    istringstream ss(aux);
    string x = "";
    getline(ss, x, ':'); ///Retirar a informašao "alfabeto de entrada:"
    x.erase(x.begin(), find_if(x.begin(), x.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
    while (!ss.eof()) {
        x = "";
        getline(ss, x, ',');
        x.erase(x.begin(), find_if(x.begin(), x.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        alfabetoDeEntrada.push_back(x);
    }
    for(int i = 0; i < alfabetoDeEntrada.size(); i++){
       // cout << alfabetoDeEntrada[i] << endl;
    }
    aux = turing[1];
    istringstream ss1(aux);
    x = "";
    getline(ss1, x, ':');
    x.erase(x.begin(), find_if(x.begin(), x.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
    while (!ss1.eof()) {
        x = "";
        getline(ss1, x, ',');
        x.erase(x.begin(), find_if(x.begin(), x.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        alfabetoDeFita.push_back(x);
    }
    for(int i = 0; i < alfabetoDeFita.size(); i++){
        //cout << alfabetoDeFita[i] << endl;
    }
    aux = turing[2];
    istringstream ss2(aux);
    while (!ss2.eof()) {
        x = "";
        getline(ss2, x, ':'); /// retira o "inicial:"
        x.erase(x.begin(), find_if(x.begin(), x.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        inicial = x;
    }
    //cout << inicial << endl;
    aux = turing[3];
    istringstream ss3(aux);
    while (!ss3.eof()) {
        x = "";
        getline(ss3, x, ':'); /// retira o "final:"
        x.erase(x.begin(), find_if(x.begin(), x.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        efinal = x;
    }
    //cout << efinal << endl;
    for (int i = 4; i < turing.size(); i++) {///Pega todo o resto
        x = "";
        x = turing[i];
        transicoes.push_back(x);
    }
    for(int i = 0; i < transicoes.size();i++){
        //cout << transicoes[i] << endl;
    }
}

void ChamaPalavra(vector<string>& palavra){
    string palavraa;
    string buff4;
    cout << "--- Maquina de Turing ---" << endl;
    cout << endl;
    cout << "Palavra : ";
    cin >> palavraa;
    cout << endl;
    for(int i = 0; i < palavraa.size(); i++){
        buff4 = "";
        buff4 = palavraa[i];
        palavra.push_back(buff4);
    }
    palavra.push_back("B");
}

#endif // FUNCOESVECTORS_H_INCLUDED
