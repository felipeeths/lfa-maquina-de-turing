#ifndef ARQUIVOS_H_INCLUDED
#define ARQUIVOS_H_INCLUDED
#include <fstream>

using namespace std;
void CarregaArq(vector<string> &turing){
    ifstream arq;
    string buff;
    arq.open("Turing.txt",ios::in);
    while(arq.good()){
        getline(arq,buff);
        turing.push_back(buff);
    }
    arq.close();
}

#endif // ARQUIVOS_H_INCLUDED
